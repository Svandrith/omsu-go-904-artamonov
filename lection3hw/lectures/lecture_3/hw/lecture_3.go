package main

import (
	"strconv"
	"strings"
)

// ISet
// Реализуйте методы интерфейса ISet -- множество целых чисел
// Оценка: 10 баллов
type ISet interface {
	//Has указывает, содержит ли множество неотрицательное значение х
	Has(x int) bool
	//Add добавляет неотрицательное значение x в множество,
	Add(x int)
	//UnionWith делает множество s равным объединению множеств s и t.
	UnionWith(t ISet)
	//String возвращает множество как строку вида "{1 2 3}"
	String() string
	//Len Возвращает количество элементов
	Len() int
	//Clear Удаляет все элементы множества
	Clear()
}

type IntSet struct {
	set []int
}

func (s *IntSet) Has(x int) bool {
	for _, item := range s.set {
		if item == x {
			return true
		}
	}
	return false
}

func (s *IntSet) Add(x int) {
	if s.Has(x) {
		return
	}
	s.set = append(s.set, x)
}

func (s *IntSet) UnionWith(t IntSet) {
	for _, item := range t.set {
		s.Add(item)
	}
}

func (s *IntSet) String() string {
	stringValues := make([]string, 0, 0)

	for _, item := range s.set {
		stringItem := strconv.Itoa(item)
		stringValues = append(stringValues, stringItem)
	}
	result := strings.Join(stringValues, ", ")
	return "{" + result + "}"
}

func (s *IntSet) Len() int {
	return len(s.set)
}

func (s *IntSet) Clear() {
	s.set = s.set[:0]
	return
}
