package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestIntSet(t *testing.T) {
	intSet := IntSet{}

	for i := 0; i < 10; i++ {
		intSet.Add(i)
	}

	assert.Equal(t, 10, intSet.Len())
	assert.Equal(t, "{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}", intSet.String())

	for i := 0; i < 10; i++ {
		assert.True(t, intSet.Has(i))
	}

	assert.False(t, intSet.Has(1000))

	intSetNew := IntSet{}

	for i := 0; i <= 5; i++ {
		intSetNew.Add(i * 2)
	}

	intSet.UnionWith(intSetNew)
	assert.Equal(t, 11, intSet.Len())
	assert.Equal(t, "{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}", intSet.String())

	intSet.Clear()
	assert.Equal(t, "{}", intSet.String())
}
